package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    /**
     * Contructor
     */
    public CleaningService(String nama){
        super(nama, "Cleaning Service");
    }

    // CleaningService ini membersihkan benda
    public void bersihkan(Benda benda){
        if (benda.getStatusCovid().equals("Positif")) {
            benda.ubahStatus("Negatif");
            benda.setPersentaseMenular(0);
            benda.resetRantaiPenular();
            jumlahDibersihkan++; // Update nilai atribut
        } else {
            benda.setPersentaseMenular(0);
            jumlahDibersihkan++; // Update nilai atribut
        }
        
    }

    // Kembalikan nilai dari atribut jumlahDibersihkan
    public int getJumlahDibersihkan(){
        return jumlahDibersihkan;
    }

    @Override
    public String toString() {
        return  "CLEANING SERVICE " + super.getNama();
    }

}