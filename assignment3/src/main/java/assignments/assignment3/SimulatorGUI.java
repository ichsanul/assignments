package assignments.assignment3;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class SimulatorGUI extends Application {
	String fontName = "Comic Sans MS"; // Untuk font
	
	// Menu terminal
	InputOutput io;
	String namaFile, namaFileOutput, outputText;
	
	// Stage GUI Manual
	Stage stageTambah;  
	World world; 

	//untuk menu tambah
	TableView table;
	ObservableList<Carrier> obsCarrier;
	String fieldNama, fieldTipe;
    
	// untuk menu daftar
    ChoiceBox objek;
    
    // untuk menu interaksikan
    ChoiceBox objek1, objek2;
    
    //untuk menu perlakuan
    ArrayList<String> listNama, listManusia, listMedis, listBenda, listPkebersihan;
    ChoiceBox cbManusia, cbMedis, cbBenda, cbPkebersihan;
    
    // untuk menu total
    ChoiceBox cb;
    
    @Override
    public void start(Stage stage) {
//		// Stage Utama : Inputfile
    	BorderPane border = appTerminal(); // memanggil fungsi appterminal
    	Scene scene = new Scene(border, 640, 480);
        stage.setScene(scene);
        stage.setTitle("Rantai Corona");
        stage.show();
    	
//      // Stage Tambah : Game mode
        world = new World();
        stageTambah = new Stage();
        TabPane tpn = new TabPane();
        Tab daftar = new Tab("Daftar Objek"); // daftar objek dan rantai 
        Tab add = new Tab("Tambahkan Objek"); 
        Tab interaksi = new Tab("Interaksikan Objek"); 
        Tab perlakuan = new Tab("Perlakuan Objek"); 
        Tab total = new Tab("Rekap Objek"); 
        
        // Agar tidak bisa diclose
        tpn.getTabs().addAll(daftar, add, interaksi, perlakuan, total);
        for (Tab tap:tpn.getTabs()) {
        	tap.setClosable(false);
        }
        
        GridPane gp1 = menuDaftar();
        daftar.setContent(gp1);
        GridPane gp2 = menuTambah();
        add.setContent(gp2);
        GridPane gp3 = menuInteraksi();
        interaksi.setContent(gp3);
        VBox vb1 = menuPerlakuan();
        perlakuan.setContent(vb1);
        VBox vb2 = menuTotal();
        total.setContent(vb2);
    
        Scene sceneTambah = new Scene(tpn, 400, 550);
        stageTambah.setTitle("Covigame V1");
        stageTambah.setScene(sceneTambah);
        stageTambah.setX(484);
        stageTambah.setY(256);
        
    }
    
    public BorderPane appTerminal() {
    	BorderPane border = new BorderPane();
    	
    	VBox vb = new VBox();
        vb.setPadding(new Insets(25, 25, 25, 25));
        vb.setSpacing(5);
        vb.setAlignment(Pos.CENTER);
        border.setCenter(vb);
        
        // Jika tombol ditekan akan muncul jendala baru untuk game 
    	ToggleButton toggleButton = new ToggleButton("Jalankan program secara manual (GUI)");
    	toggleButton.setAlignment(Pos.TOP_LEFT);
    	toggleButton.setOnAction(e -> {
    		stageTambah.show();
    	});
    	border.setTop(toggleButton);

    	// Label input file
    	Label fileLabel = new Label("Input file");
        fileLabel.setFont(Font.font(fontName,FontWeight.NORMAL,12));
        vb.getChildren().add(fileLabel);
        
        // Membuat tabpane untuk nampung tabInput
        TabPane tp = new TabPane();
        
        // tab input file
        Tab tabInput = new Tab("From File");
        tabInput.setClosable(false);
        tp.getTabs().add(tabInput);
        
        // hbox 
        HBox hb1 = new HBox();
        
        TextField file = new TextField("contoh: C:\\Input.txt");
        hb1.getChildren().add(file);
        Button impor = new Button("Import");
        hb1.getChildren().add(impor);
        Text suksesImport = new Text();
        hb1.getChildren().add(suksesImport);
        HBox.setMargin(suksesImport, new Insets(5, 5, 5, 5));
        
        // masukkan hbox ke tabInput lalu ke tp
        tabInput.setContent(hb1);
        
        // tambah tabPane
        vb.getChildren().add(tp);
        
        // 
        Button proses = new Button("Proses");
        vb.getChildren().add(proses);

        // text output
        TextArea output = new TextArea();
        output.setEditable(false);
        output.setText("Contoh output: \n\n"
        		+ "Rantai penyebaran JURNALIS Krysta: OJOL Brandies -> JURNALIS Krysta\r\n" + 
        		"JURNALIS Krysta telah menyebarkan virus COVID ke 1 objek\r\n" + 
        		"JURNALIS Krysta telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak 1 objek\r\n" + 
        		"assignments.assignment3.BelumTertularException: CLEANING SERVICE Genta berstatus negatif\r\n" + 
        		"Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: 1 kasus\r\n" + 
        		"OJOL Brandies telah menyebarkan virus COVID ke 4 objek\r\n" + 
        		"Rantai penyebaran PEKERJA JASA Rabbi: OJOL Brandies -> OJOL Joko -> PEKERJA JASA Rabbi\r\n" + 
        		"Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: 2 kasus\r\n" + 
        		"PINTU Mall telah menyebarkan virus COVID ke 0 objek\r\n" + 
        		"PETUGAS MEDIS Ariani menyembuhkan 1 manusia\r\n" + 
        		"PETUGAS MEDIS Iffat menyembuhkan 1 manusia\r\n" + 
        		"CLEANING SERVICE Genta membersihkan 1 benda\r\n" + 
        		"assignments.assignment3.BelumTertularException: PEKERJA JASA Rabbi berstatus negatif\r\n" + 
        		"Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: 2 kasus\r\n" + 
        		"");
        vb.getChildren().add(output);
        
        
        // hbox
        HBox hb = new HBox();
        TextField textOutputFile = new TextField("contoh: C:\\Output.txt");
        hb.getChildren().add(textOutputFile);
        Button export = new Button("Export");
        hb.getChildren().add(export);
        Text suksesExport = new Text();
        hb.getChildren().add(suksesExport);
        HBox.setMargin(suksesExport, new Insets(5, 5, 5, 5));
        
        vb.getChildren().add(hb);
        
        // permrosesan input teks
        impor.setOnAction(e -> {
        	namaFile = file.getText();
        	try {
        		Files.readAllLines(Paths.get(namaFile));
        		io = new InputOutput("text", namaFile, "text", "later.txt");
        		suksesImport.setFill(Color.GREEN);
        		suksesImport.setText("Sukses mengimpor file " + namaFile);
        	} catch(Exception ex) {
        		suksesImport.setFill(Color.FIREBRICK);
        		suksesImport.setText("Tidak terimpor! (Coba tambahkan directory fullnya)");
        	}
        	proses.setOnAction(e2 -> {
        		try {
        			outputText = io.runJFX();
        			output.setText(outputText);
        		} catch(Exception ex) {
        		}

        	});
        });
        
        // pembuatan file output
        export.setOnAction(e3 -> {
        	String nama = textOutputFile.getText();
    		try {
                File savean = new File(nama);
                PrintWriter pw = new PrintWriter(savean);
                String[] listOutput = outputText.split("\n");
                for (String line:listOutput) {
                    pw.println(line);
                }
                pw.close();
                suksesExport.setFill(Color.GREEN);
                suksesExport.setText("Sukses mengekspor file ke " + nama);
                }
                catch(Exception ex) {
                	suksesExport.setFill(Color.FIREBRICK);
            		suksesExport.setText("Gagal!");
                }
    	});
        return border;
    }
    
    public GridPane menuDaftar() {
    	GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setPadding(new Insets(25, 25, 25, 25));
        gp.setId("grid");
       
        // Daftar Objek
        Text sceneTitle = new Text("Daftar Objek");
        sceneTitle.setFont(Font.font(fontName,FontWeight.NORMAL,20));
        gp.add(sceneTitle, 0, 0);
        
        TableView table = new TableView();
        table.setMinWidth(300);
        table.setMaxHeight(200);
        List<Carrier> listCarrier = world.listCarrier;
        ObservableList<Carrier> obsCarrier = FXCollections.observableList(listCarrier);
        
        TableColumn column1 = new TableColumn ("Nama");
		column1.setCellValueFactory(new PropertyValueFactory<Carrier, String>("nama"));
		TableColumn column2 = new TableColumn<>("Tipe");
		column2.setCellValueFactory(new PropertyValueFactory<Carrier, String>("tipe"));
		TableColumn column3 = new TableColumn<>("Status");
		column3.setCellValueFactory(new PropertyValueFactory<Carrier, String>("StatusCovid"));
        
		table.getColumns().addAll(column1, column2, column3);
		table.setItems(obsCarrier);
		
		gp.add(table, 0, 1);
        
        Label rantai = new Label("Cari Rantai Penyebaran");
        rantai.setFont(Font.font(fontName,FontWeight.NORMAL,12));
        gp.add(rantai, 0, 2);
        
        listNama = world.getListNama();
        objek = new ChoiceBox(FXCollections.observableArrayList(listNama));
        gp.add(objek, 0, 3);
        
        Button cari = new Button("Rekap");
        cari.setFont(Font.font(fontName,FontWeight.NORMAL,12));
        gp.add(cari, 0, 4);
        
        Text actionTarget = new Text();
        actionTarget.setFont(Font.font(fontName, FontWeight.NORMAL, 12));
        gp.add(actionTarget, 0, 5);
        
        cari.setOnAction(e -> {
        	String output = "";
        	String nama = (String)objek.getValue();
        	Carrier carrier2 = world.getCarrier(nama);
        	try {
	            Carrier.cekStatus(carrier2);
	        	output = "Rantai penyebaran " + nama + ": ";
	            List<Carrier> list = carrier2.getRantaiPenular();
	            for(Carrier carrier:list) {
	                output += carrier  + " -> ";
	            }
	            output += nama +"\n";
	            actionTarget.setFill(Color.NAVY);
	            actionTarget.setText(output);
        	} catch(BelumTertularException ex) {
        		output = ex.getMessage();
        		actionTarget.setFill(Color.FIREBRICK);
        		actionTarget.setText(output);
        		
        	}
        });
  
        return gp;
    }
    
    public GridPane menuTambah() {
    	GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setPadding(new Insets(25, 25, 25, 25));
        gp.setId("grid");
       
        Text sceneTitle = new Text("Menu Tambah");
        sceneTitle.setFont(Font.font(fontName,FontWeight.NORMAL,20));
        gp.add(sceneTitle, 0, 0, 2, 1);
       
        Label nama = new Label("Nama:");
        nama.setFont(Font.font(fontName,FontWeight.NORMAL,12));
        gp.add(nama, 0, 1);
       
        TextField tulisNama = new TextField();
        tulisNama.setFont(Font.font(fontName,FontWeight.NORMAL, 12));
        gp.add(tulisNama, 1, 1);
       
        Label tipe = new Label("Tipe:");
        tipe.setFont(Font.font(fontName,FontWeight.NORMAL,12));
        gp.add(tipe, 0, 2);
        
        String[] tipeCheck = {"PEKERJA_JASA", "CLEANING_SERVICE", "JURNALIS", "OJOL",
        		"PETUGAS_MEDIS", "TOMBOL_LIFT", "PEGANGAN_TANGGA", "PINTU", "ANGKUTAN_UMUM"};
        ChoiceBox tipeObjek = new ChoiceBox(FXCollections.observableArrayList(tipeCheck));
        gp.add(tipeObjek, 1, 2);
     
        Separator spt = new Separator(Orientation.HORIZONTAL);
        gp.add(spt, 1, 3);
        
        Button btn = new Button("Tambahkan");
        btn.setFont(Font.font(fontName,FontWeight.NORMAL,12));
        gp.add(btn,1,4);

        Text actionTarget = new Text();
        actionTarget.setFont(Font.font(fontName, FontWeight.NORMAL, 12));
        gp.add(actionTarget, 1, 6);
        
        // Update semua Field Input setelah menambahkan objek
        btn.setOnAction((ActionEvent event) -> {
            fieldNama = tulisNama.getText();
            fieldTipe = (String)tipeObjek.getValue();
                if (fieldNama.equals("")){
                    actionTarget.setFill(Color.FIREBRICK);
                    actionTarget.setText("Nama tidak boleh kosong!");               
                }else{
                	try {
                		// Menambah objek carrier ke world
                		Carrier carrier = world.createObject(fieldTipe, fieldNama);
                		tulisNama.clear();
                		if( carrier != null) {
                			actionTarget.setFill(Color.GREEN);
	                        actionTarget.setText(String.format("Berhasil Menambahkan %s ",
	                        		carrier));
                		} else {
                			actionTarget.setFill(Color.FIREBRICK);
                			actionTarget.setText(fieldNama + " Sudah terdaftar");
                		}
                        // update menu daftar objek
                        //table.setItems(obsCarrier);
                        listNama = world.getListNama();
                        objek.setItems(FXCollections.observableArrayList(listNama));
                       // update menu interaksikan objek
                        objek1.setItems(FXCollections.observableArrayList(listNama));
                        objek2.setItems(FXCollections.observableArrayList(listNama));
                        // update menu perlakuan objek
                        listManusia = world.getListManusia();
                        listMedis = world.getListMedis();
                        listBenda = world.getListBenda();
                        listPkebersihan = world.getListPkebersihan();
                        cbManusia.setItems(FXCollections.observableArrayList(listManusia));
                        cbMedis.setItems(FXCollections.observableArrayList(listMedis));
                        cbBenda.setItems(FXCollections.observableArrayList(listBenda));
                        cbPkebersihan.setItems(FXCollections.observableArrayList(listPkebersihan));
                        // update menu rekap objek
                        cb.setItems(FXCollections.observableArrayList(listManusia));

                	} catch(Exception ex) {
                		actionTarget.setFill(Color.FIREBRICK);
            			actionTarget.setText("Nama tidak boleh kosong!");
                	}
                }
        });
        return gp;
    }
    
    public GridPane menuInteraksi() {
    	GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setPadding(new Insets(25, 25, 25, 25));
        gp.setId("grid");
       
        Text sceneTitle = new Text("Menu Interaksi");
        sceneTitle.setFont(Font.font(fontName,FontWeight.NORMAL,20));
        gp.add(sceneTitle, 0, 0);
        
        ArrayList<String> listNama = world.getListNama();
        objek1 = new ChoiceBox(FXCollections.observableArrayList(listNama));
        gp.add(objek1, 0, 4);
        
        objek2 = new ChoiceBox(FXCollections.observableArrayList(listNama));
        gp.add(objek2, 0, 6);

        Separator spt = new Separator(Orientation.HORIZONTAL);
        gp.add(spt, 0, 8);
        
        Button btn = new Button("Interaksikan");
        btn.setFont(Font.font(fontName,FontWeight.NORMAL,12));
        gp.add(btn,1,9);

        Text actionTarget = new Text();
        actionTarget.setFont(Font.font(fontName, FontWeight.NORMAL, 12));
        gp.add(actionTarget, 0, 10);

        btn.setOnAction(e -> {
        	String namaObjek1 = (String)objek1.getValue();
            String namaObjek2 = (String)objek2.getValue();
            Carrier objek1 = world.getCarrier(namaObjek1);
            Carrier objek2 = world.getCarrier(namaObjek2);
            try {
            	objek1.interaksi(objek2);
            	actionTarget.setFill(Color.GREEN);
            	actionTarget.setText(String.format("%s berinteraksi dengan %s",
            			objek1, objek2));
            } catch (Exception ex) {
            	actionTarget.setFill(Color.FIREBRICK);
            	actionTarget.setText("Input tidak boleh Kosong!");
            }
        });
        return gp;
    }
    
    public VBox menuPerlakuan() {
    	Text oleh = new Text("Oleh");
    	Button terapkan = new Button("Terapkan");
    	
    	listManusia = world.getListManusia();
    	listMedis = world.getListMedis();
    	listBenda = world.getListBenda();
        listPkebersihan = world.getListPkebersihan();
    	 
    	MenuItem positif = new MenuItem("Positifkan");
        MenuItem sembuhkan = new MenuItem("Sembuhkan");
        MenuItem bersihkan = new MenuItem("Bersihkan");
        // nambahkan semua menu 
        MenuButton menu = new MenuButton("Pilih Menu", null, positif, sembuhkan, bersihkan);

        //
        VBox vb = new VBox();
    	vb.setAlignment(Pos.CENTER);
        vb.setPadding(new Insets(25, 25, 25, 25));

        Text sceneTitle = new Text("Menu Perlakuan");
        sceneTitle.setFont(Font.font(fontName,FontWeight.NORMAL,20));
        vb.getChildren().add(sceneTitle);
        
        cbManusia = new ChoiceBox(FXCollections.observableArrayList(listManusia));
        cbMedis = new ChoiceBox(FXCollections.observableArrayList(listMedis));
        cbBenda = new ChoiceBox(FXCollections.observableArrayList(listBenda));
        cbPkebersihan = new ChoiceBox(FXCollections.observableArrayList(listPkebersihan));
        
        Text output = new Text();
        vb.getChildren().add(output);
        vb.getChildren().add(menu); 
        
        // untuk menu positifkan
        positif.setOnAction(event -> {
        	try {
        	vb.getChildren().removeAll(cbMedis, cbBenda, cbPkebersihan, oleh);
        	vb.getChildren().add(cbManusia);
        	vb.getChildren().add(terapkan);
        	
        	terapkan.setOnAction(e -> {
        		Carrier nama = world.getCarrier((String)cbManusia.getValue());
        		try {
	        		nama.ubahStatus("Positif");
	        		output.setFill(Color.CYAN);
	        		output.setText(nama + " berstatus positif COVID");
        		} catch (Exception ex) {
        			output.setText("Gagal atau tidak terpengaruh!");
            		output.setFill(Color.FIREBRICK);
        		}
        	});
        	} catch(Exception ex) {}
        });
        
        // untuk menu sembuhkan
        sembuhkan.setOnAction(event -> {
        	try {
        	vb.getChildren().removeAll(cbManusia, cbMedis, cbBenda, cbPkebersihan, oleh, terapkan);	
        	vb.getChildren().add(cbManusia);
        	vb.getChildren().add(oleh);
        	vb.getChildren().add(cbMedis);
        	vb.getChildren().add(terapkan);
        	
        	terapkan.setOnAction(e -> {
        		Manusia namaMan = (Manusia)world.getCarrier((String)cbManusia.getValue());
            	PetugasMedis namaMed = (PetugasMedis)world.getCarrier((String)cbMedis.getValue());
            	try {
            		namaMed.obati(namaMan);
            		output.setText(namaMed + " berhasil mengobati " + namaMan);
            	} catch (Exception ex) {
            		output.setText("Gagal atau tidak terpengaruh!");
            		output.setFill(Color.FIREBRICK);
            	}
        	});
        	} catch(Exception ex) {}
        });
        
        // untuk menu bersihkan
        bersihkan.setOnAction(event -> {
        	try {
        	vb.getChildren().removeAll(cbManusia, cbMedis, cbBenda, cbPkebersihan, oleh, terapkan);
        	vb.getChildren().add(cbBenda);
        	vb.getChildren().add(oleh);
            vb.getChildren().add(cbPkebersihan);
            vb.getChildren().add(terapkan);
            
            terapkan.setOnAction(e -> {
	            Benda benda = (Benda)world.getCarrier((String)cbBenda.getValue());
	        	CleaningService cs = (CleaningService)world.getCarrier((String)cbPkebersihan.getValue());
	        	try {
	        		cs.bersihkan(benda);
	        		output.setText(cs + " berhasil membersihkan " + benda);
	        	} catch (Exception ex) {
	        		output.setText("Gagal atau tidak terpengaruh!");
            		output.setFill(Color.FIREBRICK);
	        	}
            });
        	} catch(Exception ex) {}
        });
        
        // Button
        terapkan.setOnAction(e -> {
        	try {
        	vb.getChildren().removeAll(cbManusia, cbMedis, cbBenda, cbPkebersihan, oleh, terapkan);
        	} catch(Exception ex) {
        		output.setFill(Color.AZURE);
        		output.setText("OK");
        	}
        });
        
        return vb;
    }
    
    public VBox menuTotal() {
    	VBox vb = new VBox();
    	vb.setAlignment(Pos.CENTER);
        vb.setPadding(new Insets(25, 25, 25, 25));
        
        Text sceneTitle = new Text("Menu Total");
        sceneTitle.setFont(Font.font(fontName,FontWeight.NORMAL,20));
        vb.getChildren().add(sceneTitle);
        
        MenuItem totalKasus = new MenuItem("Total Kasus Dari...");
        MenuItem aktifKasus = new MenuItem("Total Kasus Aktif Dari...");
        MenuItem totalPasienSembuh = new MenuItem("Total Sembuh Petugas Medis...");
        MenuItem totalBendaBersih = new MenuItem("Total Bersih Cleaning Service...");
     
        MenuButton menu = new MenuButton("Pilih Menu Lain...", null, totalKasus, aktifKasus
        		,totalPasienSembuh, totalBendaBersih);
        
        // total sembuh manusia
        Button totalSembuh = new Button("Total Sembuh Seluruh Manusia");
        Text outputTotalSembuh = new Text();
        vb.getChildren().add(totalSembuh);
        vb.getChildren().add(outputTotalSembuh);
        VBox.setMargin(totalSembuh, new Insets(50, 50, 50, 50));
        
        totalSembuh.setOnAction(e -> {
        	int jumlahManusiaSembuh = Manusia.getJumlahSembuh();
            String output = String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %s kasus",
                    jumlahManusiaSembuh);
            outputTotalSembuh.setFill(Color.GREEN);
            outputTotalSembuh.setText(output);
        });
        
        // Menu total bagian bawahnya
        cb = new ChoiceBox();
        vb.getChildren().add(menu);
        vb.getChildren().add(cb);
        
        Button ok = new Button("Telusuri");
        vb.getChildren().add(ok);
        
        // total kasus
        totalKasus.setOnAction(e -> {
        	listNama = world.getListNama();
    		cb.setItems(FXCollections.observableArrayList(listNama));
        	ok.setOnAction(e1 -> {
        		Carrier nama = world.getCarrier((String)cb.getValue());
                int jumlahKasus = nama.getTotalKasusDisebabkan();
                String output = nama + " telah menyebarkan virus COVID ke "+
                		jumlahKasus +" objek\n";
                outputTotalSembuh.setFill(Color.GREEN);
                outputTotalSembuh.setText(output);
        	});
        	
        });
        
        // aktif kasus
        aktifKasus.setOnAction(e -> {
        	listNama = world.getListNama();
    		cb.setItems(FXCollections.observableArrayList(listNama));
        	ok.setOnAction(e1 -> {
        		Carrier nama = world.getCarrier((String)cb.getValue());
                int jumlahKasusAktif = nama.getAktifKasusDisebabkan();
                String output = nama + " telah menyebarkan virus COVID dan masih "+
                		"teridentifikasi positif sebanyak " + jumlahKasusAktif +" objek\n";
                outputTotalSembuh.setFill(Color.GREEN);
                outputTotalSembuh.setText(output);
        	});
        	
        });
        
        // total pasien sembuh
        totalPasienSembuh.setOnAction(e -> {
        	listMedis = world.getListMedis();
        	cb.setItems(FXCollections.observableArrayList(listNama));
        	ok.setOnAction(e1 -> {
        		Carrier nama = world.getCarrier((String)cb.getValue());
        		PetugasMedis petugas = (PetugasMedis)(nama);
        		String output = String.format("%s menyembuhkan %d manusia", petugas, petugas.getJumlahDisembuhkan()) + "\n";
        		outputTotalSembuh.setFill(Color.GREEN);
        		outputTotalSembuh.setText(output);
        	});
        	
        });
        
        // total benda bersih
        totalBendaBersih.setOnAction(e -> {
        	listBenda = world.getListBenda();
        	cb.setItems(FXCollections.observableArrayList(listBenda));
        	ok.setOnAction(e1 -> {
        		Carrier nama = world.getCarrier((String)cb.getValue());
        		CleaningService petugas = (CleaningService)(nama);
        		String output = String.format("%s membersihkan %d benda", petugas, petugas.getJumlahDibersihkan()) + "\n";
        		outputTotalSembuh.setFill(Color.GREEN);
        		outputTotalSembuh.setText(output);
        	});
        });
        return vb;
    }
    
    public static void main(String[] args) {
        launch();
    }
}
