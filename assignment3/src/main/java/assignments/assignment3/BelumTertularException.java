package assignments.assignment3;

public class BelumTertularException extends Exception	{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Throw exception ini apabila melakukan method khusus status Positif Pada
	 * Menausia yang berstatus negatif
	 */
	public BelumTertularException(String errorMessage)	{
		super(errorMessage);
	}
}