package assignments.assignment3;

import java.util.ArrayList;

public abstract class Carrier{
    // Atribut
    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private ArrayList<Carrier> rantaiPenular;

    /**
     * Contructor
     */
    public Carrier(String nama,String tipe){
        this.nama = nama;
        this.tipe = tipe;
        statusCovid = new Negatif(); //set objek awal dengan status negatif
        rantaiPenular = new ArrayList<>();
    }

    // Kembalikan nilai dari atribut nama
    public String getNama(){
        return this.nama;
    }

    // Kembalikan nilai dari atribut tipe
    public String getTipe(){
        
        return this.tipe;
    }

    // Kembalikan nilai dari atribut statusCovid
    public String getStatusCovid(){
        return this.statusCovid.getStatus();
    }

    // Kembalikan nilai dari atribut aktifKasusDisebabkan
    public int getAktifKasusDisebabkan(){
        return this.aktifKasusDisebabkan;        
    }

    // Kembalikan nilai dari atribut totalKasusDisebabkan
    public int getTotalKasusDisebabkan(){
        return this.totalKasusDisebabkan;
    }

    // Kembalikan nilai dari atribut rantaiPenular
    public ArrayList<Carrier> getRantaiPenular(){
        return this.rantaiPenular;
    }

    // Mengubah atribut dari statusCovid
    public void ubahStatus(String status){
        if (status.equals("Positif")) {
            this.statusCovid = new Positif();
        } else {
            this.statusCovid = new Negatif();
        }
    }

    // Objek ini berinteraksi dengan objek lain
    public void interaksi(Carrier lain){
        if (this.statusCovid.getStatus().equals("Negatif")) {
            if (lain.statusCovid.getStatus().equals("Positif")) {
                lain.statusCovid.tularkan(lain, this);
            }
        } else {
            if (lain.getStatusCovid().equals("Negatif")) {
                this.statusCovid.tularkan(this, lain);
            }
        }
    }

    /*
    // Menambahkan carrier lain ke atribut rantaiPenular objek ini
    public void tambahRantai(Carrier lain) {
        this.rantaiPenular.add(lain);
    }
    */

    // Menambahkan list rantaiPenular lain ke atribut rantaiPenular objek ini
    public void tambahRantai(Carrier penular) {
        ArrayList<Carrier> listRantai = penular.rantaiPenular;
        for (Carrier carrier:listRantai) {
            this.rantaiPenular.add(carrier);
        }

        this.rantaiPenular.add(penular);
    }

    // Menambah kasus aktif dan total disebabkan apabila berinteraksi dengan status negatif
    public void tambahKasusDisebabkan() {
        for (Carrier carrier:rantaiPenular) {
            if (carrier == this){
                continue;
            } else {
            carrier.totalKasusDisebabkan++;
            carrier.aktifKasusDisebabkan++;
            }
        }
        this.totalKasusDisebabkan++;
        this.aktifKasusDisebabkan++;
    }

    // Mereset rantai penular saat orang ini disembuhkan
    public void resetRantaiPenular() {
        for (Carrier carrier:rantaiPenular) {
            carrier.aktifKasusDisebabkan--;
        }
        this.rantaiPenular.clear();
    }

    // Throw Belum Tertular Exception 
    public static void cekStatus(Carrier objek) throws BelumTertularException {
        if(objek.getStatusCovid().equals("Negatif")) {
            throw new BelumTertularException(objek + " berstatus negatif");
        }
    }

    public abstract String toString();
	

}
