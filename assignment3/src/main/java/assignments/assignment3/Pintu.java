package assignments.assignment3;

public class Pintu extends Benda{
    
    /**
     * Constructor
     */
    public Pintu(String name){
        super(name, "Pintu");
    }

    /**
     * Menambah persentase penularan sebanyak 30
     */
    @Override
    public void tambahPersentase() {
        super.persentaseMenular += 30;
    }

    @Override
    public String toString() {
        return "PINTU " + super.getNama();
    }
}