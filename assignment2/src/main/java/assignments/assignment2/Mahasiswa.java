package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
    	this.npm = npm;
    	this.nama = nama;
    	this.komponenPenilaian = komponenPenilaian;
        // buat constructor untuk Mahasiswa.
        // komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for (int i = 0; i < komponenPenilaian.length; i++) {
        	if (komponenPenilaian[i].getNama().equalsIgnoreCase(namaKomponen)) {
        		return komponenPenilaian[i];
        	}
        }
        // kembalikan KomponenPenilaian yang bernama namaKomponen.
        // jika tidak ada, kembalikan null atau lempar sebuah Exception.
        return null;
    }

    public String getNpm() {
        // kembalikan NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
    	String strAkhir = "";
    	double nilaiAkhir = 0;
    	for (int i = 0; i < komponenPenilaian.length; i++) {
    		strAkhir += komponenPenilaian[i].toString() + "\n";
    		nilaiAkhir += komponenPenilaian[i].getNilai();
    	}
    	strAkhir += "Nilai akhir: " + String.format("%.2f", nilaiAkhir) + "\n";
    	strAkhir += "Huruf: " + getHuruf(nilaiAkhir) + "\n";
    	strAkhir += getKelulusan(nilaiAkhir) + "\n";
        // kembalikan rekapan sesuai dengan permintaan soal.
        return strAkhir;
    }

    public String toString() {
        // kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return this.npm + " - " + this.nama;
    }

    public String getDetail() {
    	String strAkhir = "";
    	double nilaiAkhir = 0;
    	for (int i = 0; i < komponenPenilaian.length; i++) {
    		strAkhir += komponenPenilaian[i].getDetail() + "\n"; //salah
    		nilaiAkhir += komponenPenilaian[i].getNilai();
    	}
    	strAkhir += "Nilai akhir: " + String.format("%.2f", nilaiAkhir) + "\n";
    	strAkhir += "Huruf: " + getHuruf(nilaiAkhir) + "\n";
    	strAkhir += getKelulusan(nilaiAkhir) + "\n";
        // kembalikan detail dari Mahasiswa sesuai permintaan soal.
        return strAkhir;
    }

    @Override
    public int compareTo(Mahasiswa other) {
    	return this.npm.compareTo(other.getNpm());
        // definisikan cara membandingkan seorang mahasiswa dengan mahasiswa lainnya.
        // bandingkan NPM-nya, String juga punya method compareTo.
    }
}
