package assignments.assignment2;

public class ButirPenilaian {
    private static final double PENALTI_KETERLAMBATAN = 20.0;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
    	this.nilai = nilai;
    	this.terlambat = terlambat;
    }

    public double getNilai() {
    	double nilaiAkhir;
    	if (this.terlambat == true) {
			nilaiAkhir = this.nilai - (PENALTI_KETERLAMBATAN / 100.0 * this.nilai);
			if (nilaiAkhir >= 0) {
				return nilaiAkhir;
			} else {
				return 0.00;
			}	
    	}
        
        return (this.nilai >= 0) ? this.nilai : 0.00;
    }

    @Override
    public String toString() {
    	if (terlambat) {
    		return String.format("%.2f (T)", this.getNilai());
    	}
    	return String.format("%.2f", this.getNilai());
    }
}
